from mongoengine import *
from dateutil import *
connect('autoJAV', host='192.168.1.1', port=27017)

class jav(Document):
    javID = StringField(required=True)
    title = StringField(max_length=1000)
    releaseDate = DateTimeField()#Datetype?
    length = IntField()
    director = StringField(max_length=100)
    maker = StringField()
    label = StringField()
    javLibraryRating = FloatField()
    userRating  = FloatField()
    scrapeDate = StringField()
    genres = ListField()
    actors = ListField()
    #video res
    #video bitrate
    #video codec
    #audio codec
    #video size
    #locationPath
    #screenspath
    #coverpath
    #sample (yes/no)
    #sample quality
    #timesWatched #started?

jav_1 = jav(
    javID ='SDMT-311',
    title = '',
    genres = ['',''],
    label = '',
    date = '2011-01-20',
    length = '130'
)

jav_1.save()    #performs an insert
