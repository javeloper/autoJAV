import os
import shutil #if moving file from a system to a different system, will copy, and then delete old file
from pymediainfo import MediaInfo

javDownloadWatchDir = ""
javBaseDir = "test/jav/"
javDir = "test/javFinal/" #Where you store your jav, eg, your NAS (final resting place of this process)

#if filename.endswith((video_file_extensions)):
#    return True

#old_file = os.path.join(".", "Gvg579mp4")
#new_file = os.path.join(".", "Gvg579")
#os.rename(old_file, new_file)

#os.path.getsize(path)	to get filesizes for db
#file extensions and size to go in DB

#needs to check for is directory or is file to know what needs folder creation and moving after renaming

#import os
#from os import path
#files = [f for f in os.listdir(dirToScreens) if path.isfile(f)]

#import os
#[os.rename(f, f.replace('_', '-')) for f in os.listdir('.') if not f.startswith('.')]

#for filename in os.listdir("."): #
#	if filename.endswith("mp4") or filename.endswith(".py"):
#		print(os.path.join(filename))

#for filename in os.listdir("."): #
#	print(os.path.join(filename))
#	for i in range(0,len(filename)):
#		print filename[i]
#		if filename[i] == "-":
#			print "success"

#method
#for each file in folder not marked as owned in DB, (organiser job, should just be passed original filepaths)
#look for a single hyhen in file name, then look left and right of file name for letters (left until either a symbol or start of file name)
#or right for numbers until either hit symbol or file extension
#if less than 3 digits, append a zero
#if folder, enter, delete everything but biggest file / video file, and rename video with javID string
#if file, create folder, name it javID string and move jav movie within it

#return javID
#print(os.listdir(javBaseDir))

for filename in os.listdir(javBaseDir):
	print(filename)
	#if filename == True:	#if not in dB check
	if os.path.isfile(javBaseDir + filename) == True:
		print("File!")
		renameFile,fileExtension = os.path.splitext(filename)
		fileInfo = MediaInfo.parse(javBaseDir + filename)
			for track in fileInfo.tracks:
    		if track.track_type == "Video":
				if renameFile.find("-") >= 0:
					hyphenLocation = [pos for pos, char in enumerate(renameFile) if char == "-"]
					hyphenLocation = hyphenLocation[::-1]
					for i in range(len(hyphenLocation)):
						javIDNumber = renameFile[(hyphenLocation[i]+1):]
						if (len(javIDNumber) >= 3) and (javIDNumber[:3].isdigit() == True):
							javIDNumber = javIDNumber[:3]
							print("javIDNumber: " + str(javIDNumber))
							javIDLettersTemp = renameFile[:(hyphenLocation[i])]
							print(javIDLettersTemp)
							javIDLettersTemp = javIDLettersTemp[::-1]
							javIDLetters = ""
							while True:
								for char in javIDLettersTemp:
									if char.isalpha() == False:
										break
									print(char, char.isalpha())
									#need to append to string, rotate, add hyphen, add javID, sanity check that ID is less than ABNOMAL?
									javIDLetters = javIDLetters + char
								javIDLetters = javIDLetters[::-1]
								renameFile = javIDLetters.upper() + "-" + javIDNumber
								print(renameFile)
								print("FIN")
								break
							os.mkdir(javDir + renameFile)
							#move video to new folder
							shutil.move(javBaseDir + filename, javDir + renameFile + fileExtension)
							#add a rename log

						#flip string, nest
				#elif:




	if os.path.isdir(javBaseDir + filename) == True:
		directoryName = filename
		print("Directory!")
		renameFile = str(directoryName)
		if renameFile.find("-") >= 0:
			hyphenLocation = [pos for pos, char in enumerate(renameFile) if char == "-"]
			hyphenLocation = hyphenLocation[::-1]
			for i in range(len(hyphenLocation)):
				javIDNumber = renameFile[(hyphenLocation[i]+1):]
				if (len(javIDNumber) >= 3) and (javIDNumber[:3].isdigit() == True):
					javIDNumber = javIDNumber[:3]
					print("javIDNumber: " + str(javIDNumber))
					javIDLettersTemp = renameFile[:(hyphenLocation[i])]
					print(javIDLettersTemp)
					javIDLettersTemp = javIDLettersTemp[::-1]
					javIDLetters = ""
					while True:
						for char in javIDLettersTemp:
							if char.isalpha() == False:
								break
							javIDLetters = javIDLetters + char
						javIDLetters = javIDLetters[::-1]
						#sanity check that ID is less than ABNOMAL?
						renameFile = javIDLetters.upper() + "-" + javIDNumber
						print(renameFile)
						break
					if renameFile != directoryName:	#(does directory already exist with this name?)
						os.mkdir(javDir + renameFile)
					#need to check for if there mutliple video files
					for nestedFile in os.listdir(javBaseDir + directoryName):
						print(nestedFile)
						renameFile,fileExtension = os.path.splitext(nestedFile)
						fileInfo = MediaInfo.parse(nestedFile)
							for track in fileInfo.tracks:
				    		if track.track_type == "Video":
								shutil.move(javBaseDir + directoryName + '/' + nestedFile, javDir + renameFile + fileExtension)
					#move all files in folder that end with a video extension
					#add a rename log
				#flip string, nest
		#elif:


## Collect media data in this step ? OR afterwards?
## At least collect file name, and file path details
## Hand them onto to next step (organise)
