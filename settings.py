#settings files
#filepaths
javWatchDir = "test/jav/"

#schedule
auto_organise = False #set to true after first scrape and dry run?
#Do i need to check directory on timer, or just use a directory watch function?

#javlibrary
JAVLibrary_username = "test"
JAVLibrary_password = "secret"

#mongoDB
mongoDB_ip = "192.168.1.1"
mondoDB_port = "27000"
mongoDB_username = "default"
mongoDB_password = "default"

#jackett
jackett_ip = "192.168.1.1"
jackett_port = "9001"
jackett_API = ""
#jackett time of day, when to scrape?

#metadata preferences
download_R18_Trailers = True #grabs trailers from r18.com if available
trailer_quality = 'hi' #accepts high or low I think?
#media preferences
reencode_high_bitrate = True
reencode_above_bitrate = 10000 #(Kbs)
ffmpeg_settings = ""
automatically_upgrade_bitrate = False #Upgrade to a higher bitrate movie if available
upgrade_below_bitrate = 8000 #(Kbs)
automatically_upgrade_codec = False #Upgrade to a higher bitrate movie if available
codecs_to_upgrade = ('.avi','.mov','.wmv') #codecs/containers we want to upgrade from

darkui = True
