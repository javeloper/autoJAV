import cfscrape
import wget
import os
import json

javBaseDir = "python/jav/"

scraper = cfscrape.create_scraper()  # returns a CloudflareScraper instance
url = 'http://www.javlibrary.com/en/vl_searchbyid.php?keyword=SDMT-311'

response = scraper.get(url)

from bs4 import BeautifulSoup

html_soup = BeautifulSoup(response.text, 'html.parser')
type(html_soup)
#print html_soup.title.text
#create scrape object here
javJSON = {}
TITLE = html_soup.find("div", {"id": "video_title"})
javJSON['title'] = str(TITLE.h3.text)
ranURL = html_soup.find("span", {"id": "subscribed"})
finURL = ranURL.find('a').get('href')
repURL = finURL.replace('userswanted.php?', '')
javJSON['repURL'] = str(repURL)

pic = html_soup.find("div", {"id": "video_jacket"})
#picURL = pic.find("img", {"id": "video_jacket_img"})
picURL = pic.find_all('img')[0].get('src')
picURL = ("http:{}".format(picURL))
javJSON['picURL'] = str(picURL)
#grab pic extension for save as

#contents = html_soup.find_all('div', id_ = 'video_id')
contents = html_soup.find_all('div', class_ = 'item')
#contents = html_soup.find_all("div", {"id": "video_id"})

javID = contents[0].find('td', class_ = 'text')
javJSON['javID'] = str(javID.text)

javDATE = contents[1].find('td', class_ = 'text')
javJSON['releaseDate'] = javDATE.text

javLength = contents[2].find('span', class_ = 'text')
javJSON['length'] = javLength.text

javDirector = contents[3].find('td', class_ = 'text')
javJSON['director'] = javDirector.text

javMaker = contents[4].find('td', class_ = 'text')
javJSON['maker'] = javMaker.text

javLabel = contents[5].find('td', class_ = 'text')
javJSON['label'] = javLabel.text

javLibraryRating = contents[6].find('span', class_ = 'score').text
javJSON['javLibraryRating'] = javLibraryRating

genres = contents[7].find('td', class_ = 'text')
javGenres = genres.find_all('span', class_ = 'genre')
javJSON['genres']=[]
for i in range(0,len(javGenres)):
	javJSON['genres'].append(javGenres[i].text)

cast = contents[8].find('td', class_ = 'text')
javJSON['actors']=[]
javCast = cast.find_all('span', class_ = 'star')
for i in range(0,len(javCast)):
	javJSON['actors'].append(javCast[i].text)

dl_path = javBaseDir + javID.text
if not os.path.exists(dl_path):
	print "path doesn't exist. trying to make"
	os.makedirs(dl_path)

#locationPath = javBaseDir + javID.text +  '/' + javID.text + ".jpg"
#print locationPath
#wget.download(picURL, str(locationPath))

print("javJSON")
print(javJSON)
